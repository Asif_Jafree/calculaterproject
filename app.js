let container = document.querySelector('.container');
console.log(container);
//heading Div
let headingDiv = document.createElement('div');
headingDiv.className = 'heading';
headingDiv.innerText = 'Calculator';
container.appendChild(headingDiv);

//Writing Space Div
let writingSpaceDiv = document.createElement('div');
writingSpaceDiv.className = 'write';
writingSpaceDiv.innerText = '';
container.appendChild(writingSpaceDiv);

//All button div
let buttonDiv = document.createElement('div');
buttonDiv.className = 'buttonDiv';
container.appendChild(buttonDiv);

let div1 = document.createElement('div');
div1.className = 'div';
buttonDiv.appendChild(div1);

let btn1 = document.createElement('button');
btn1.innerText = 'AC';
btn1.className = 'buttons';
btn1.id = 'btn1';
div1.appendChild(btn1);
let btn2 = document.createElement('button');
btn2.innerText = '+/-';
btn2.id = 'btn2';
btn2.className = 'buttons';
div1.appendChild(btn2);
let btn3 = document.createElement('button');
btn3.innerText = '%';
btn3.id = 'btn3';
btn3.className = 'buttons';
div1.appendChild(btn3);
let btn4 = document.createElement('button');
btn4.innerText = '/';
btn4.id = 'btn4';
btn4.className = 'buttons';
div1.appendChild(btn4);

let div2 = document.createElement('div');
div2.className = 'div';
buttonDiv.appendChild(div2);

let btn5 = document.createElement('button');
btn5.innerText = '7';
btn5.id = 'btn5';
btn5.className = 'buttons';
div2.appendChild(btn5);
let btn6 = document.createElement('button');
btn6.innerText = '8';
btn6.id = 'btn6';
btn6.className = 'buttons';
div2.appendChild(btn6);
let btn7 = document.createElement('button');
btn7.innerText = '9';
btn7.id = 'btn7';
btn7.className = 'buttons';
div2.appendChild(btn7);
let btn8 = document.createElement('button');
btn8.innerText = '*';
btn8.id = 'btn8';
btn8.className = 'buttons';
div2.appendChild(btn8);

let div3 = document.createElement('div');
div3.className = 'div';
buttonDiv.appendChild(div3);

let btn9 = document.createElement('button');
btn9.innerText = '4';
btn9.id = 'btn9';
btn9.className = 'buttons';
div3.appendChild(btn9);
let btn10 = document.createElement('button');
btn10.innerText = '5';
btn10.id = 'btn10';
btn10.className = 'buttons';
div3.appendChild(btn10);
let btn11 = document.createElement('button');
btn11.innerText = '6';
btn11.id = 'btn11';
btn11.className = 'buttons';
div3.appendChild(btn11);
let btn12 = document.createElement('button');
btn12.innerText = '-';
btn12.id = 'btn12';
btn12.className = 'buttons';
div3.appendChild(btn12);

let div4 = document.createElement('div');
div4.className = 'div';
buttonDiv.appendChild(div4);

let btn13 = document.createElement('button');
btn13.innerText = '1';
btn13.id = 'btn13';
btn13.className = 'buttons';
div4.appendChild(btn13);
let btn14 = document.createElement('button');
btn14.innerText = '2';
btn14.id = 'btn14';
btn14.className = 'buttons';
div4.appendChild(btn14);
let btn15 = document.createElement('button');
btn15.innerText = '3';
btn15.id = 'btn15';
btn15.className = 'buttons';
div4.appendChild(btn15);
let btn16 = document.createElement('button');
btn16.innerText = '+';
btn16.id = 'btn16';
btn16.className = 'buttons';
div4.appendChild(btn16);

let div5 = document.createElement('div');
div5.className = 'div';
buttonDiv.appendChild(div5);

let btn17 = document.createElement('button');
btn17.innerText = '0';
btn17.id = 'btn17';
btn17.className = 'specialButton';
div5.appendChild(btn17);
let btn18 = document.createElement('button');
btn18.innerText = '.';
btn18.id = 'btn18';
btn18.className = 'buttons';
div5.appendChild(btn18);
let btn19 = document.createElement('button');
btn19.innerText = '=';
btn19.id = 'btn19';
btn19.className = 'buttons';
div5.appendChild(btn19);

let var1 = '';
let var2 = '';
let operator = '';
let ans = '0';

function isOperator(value) {
  return value == '+' || value == '-' || value == '*' || value == '/';
}
function evaluateExpression() {
  buttonDiv.addEventListener('click', (e) => {
    let text = writingSpaceDiv.innerHTML;
    let value = e.target.innerText;
    if (isOperator(value)) {
      operator = value;
      var1 = parseFloat(text);
      writingSpaceDiv.innerHTML = var1;
    } else if (value === 'AC') {
      writingSpaceDiv.innerHTML = '';
    } else if (value == '+/-') {
      var1 = parseFloat(text);
      var1 = -1 * var1;
      writingSpaceDiv.innerHTML = var1;
    } else if (value == '%') {
      var1 = parseFloat(text);
      var1 = var1 / 100;
      writingSpaceDiv.innerHTML = var1;
    } else if (value == '.') {
      if (text.length && !text.includes('.')) {
        display.textContent = text + '.';
      }
    } else if (value == '=') {
      var2 = parseFloat(text);
      var result = eval(var1 + ' ' + operator + ' ' + var2);
      if (result) {
        writingSpaceDiv.innerHTML = result;
        var1 = result;
        ans=result;
        operator = '';
        var2 = '';
      }
    } else if (operator != '' && var2 == '') {
      writingSpaceDiv.innerHTML = value;
      var2 = parseFloat(text);
    } else {
      writingSpaceDiv.innerHTML += value;
    }
  });
}

evaluateExpression();
